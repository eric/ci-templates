#!/bin/bash
#
# Build a minimalist image capable of qemu and boot2container

B2C_URL=$1

set -e
set -x

dnf install -y --setopt=install_weak_deps=False \
    genisoimage \
    usbutils \
    rsync \
    qemu-img \
    python3-click \
    python3-rich

mkdir -p /app

cp bootstrap/vmctl /app/vmctl

curl -L -o /app/boot2container $B2C_URL
chmod +x /app/boot2container

# download the current released kernel and initramfs
/app/boot2container --workdir /app --dry-run || true

# clean up b2c
rm -f /app/.b2c_env \
    /app/b2c_post.sh \
    /app/cache.qcow2
